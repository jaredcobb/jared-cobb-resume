# Jared Cobb Resume #

A simple resume built using the Zurb Foundation framework, Webpack, and some custom Sass

### See It ###

Hosted at https://www.jaredcobb.com/resume

### Setup ###

* Tooling setup: `npm install`
* Running a dev server (http://localhost:8080): `npm run dev`
* Run the build: `npm run build`